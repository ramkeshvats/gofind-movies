from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib.auth import views as authentication_views


# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^api/', include('movies.urls')),
    url(r'^accounts/login', authentication_views.login, {'template_name':
        'login.html'}, name='login'),
   url(r'^$', authentication_views.login, {'template_name':
        'login.html'}, name='login')
)

urlpatterns += patterns(
        'django.views.static', (r'^media/(?P<path>.*)$', 'serve', {
            'document_root': settings.MEDIA_ROOT,
            'show_indexes': True
        }))
