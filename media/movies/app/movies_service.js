
(function(angular, _) {
  angular.module('movies').factory('MoviesService', [
    '$http' , moviesService
  ]);
  function moviesService($http)
  {
    function Movie(movieInfo){
      this.director_name = movieInfo.director_name || '';
      this.genre = movieInfo.genre || '';
      this.id = movieInfo.id || '';
      this.imdb_score = movieInfo.imdb_score || 0;
      this.name = movieInfo.name || '';
      this.popularity = movieInfo.popularity || 0;
    }

    Movie.prototype = {
      serialize: function () {
        return {
          name: this.name,
          imdb_score: this.imdb_score,
          popularity: this.popularity,
          director_name: this.director_name,
          genre: this.genre
        }
      },
      validate: function(){
        var errors = [];
        if (!this.name){
          errors.push('Please enter name of movie.');
        }
        if (!this.director_name){
          errors.push('Please enter Director name for movie.');
        }
        if (this.imdb_score > 10){
          errors.push('Please enter imdb score less than or equal to 10.');
        }
        if (this.popularity > 100){
          errors.push('Please enter popularity score less than or equal' +
            ' to 100.');
        }
        return errors.join(' ');
      },
      create: function(){
        var postData = this.serialize();
        return $http.post('/api/movies/', postData)
          .then(
            function(response) {
              return response.data;
            },
            function(errResponse) {
              console.error('Could not get config!');
            }
          );
      },
      update: function () {
        var postData = this.serialize();
        return $http.put('/api/movies/' + this.id, postData)
          .then(
            function(response) {
              return response.data;
            },
            function(errResponse) {
              console.error('Could not get config!');
            }
          );
      },
      retrieve: function () {
        var postData = this.serialize();
        return $http.get('/api/movies/' + this.id, postData)
          .then(
            function(response) {
              return response.data;
            },
            function(errResponse) {
              console.error('Could not get config!');
            }
          );
      },
      delete: function(){
        return $http.delete('/api/movies/' + this.id)
          .then(
            function(response) {
              return response.data;
            },
            function(errResponse) {
              console.error('Could not get config!');
            }
          );
      }
    };

    return {
      Movie: Movie
    }
  }
})(window.angular, window._);
