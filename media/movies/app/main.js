
(function(angular, _) {
  angular.module('movies').controller('mainCtrl', [
    '$scope', '$uibModal', '$timeout', '$http', 'MoviesService', mainCtrl
  ]);
  function mainCtrl($scope, $uibModal, $timeout, $http, MoviesService)
  {
    var vm = this;
    vm.openCreateModal = openCreateModal;
    vm.openEditModal = openEditModal;
    vm.onDelete = onDelete;
    vm.resetMessages = resetMessages;
    vm.Search = Search;
    vm.searchQuery = '';
    vm.dialogModes = {
      CREATE : 1,
      UPDATE: 2
    };

    vm.message = {
      success: false,
      error: false,
      text: ''
    };

    function Search() {
      $http.get('/api/movies/?q=' + vm.searchQuery).then(function(response) {
        vm.movies = response.data.movies_list;
      }, function(errResponse) {
        console.error('Something wrong happen');
      });
    }

    function __init() {
      $http.get('/api/movies/').then(function(response) {
        vm.movies = response.data.movies_list;
        vm.isStaffUser = response.data.is_staff_user
      }, function(errResponse) {
        console.error('Something wrong happen');
      });
    }

    __init();

    function openCreateModal(){
      var dialogSettings = {
        saveButtonText: 'Add new',
        titleText: 'Movie'
      };
      var successMessage = 'Movie has been added successfully.';
      openModal(new MoviesService.Movie({}), dialogSettings, successMessage, vm.dialogModes.CREATE);
    }

    function openEditModal(movieId){
      var dialogSettings = {
        saveButtonText: 'Update Movie Info',
        titleText: 'Edit Movie'
      };
      var successMessage = 'Movie information has been updated successfully.';
      $http.get('/api/movies/'+ movieId).then(function(response) {
        var movieObj = new MoviesService.Movie(response.data);
        openModal(
          movieObj,
          dialogSettings, successMessage, vm.dialogModes.UPDATE);
      }, function(errResponse) {
        console.error('Something wrong happen');
      });
    }

    function openModal(movieInfo, dialogSettings, successMessage, dialogMode){
      var modal = $uibModal.open({
        templateUrl: '/media/movies/app/movie_dialog.html',
        controller: 'MoviesCtrl',
        controllerAs: 'dialog',
        resolve: {
          movieInfo: movieInfo,
          dialogSettings: dialogSettings
        }
      });

      modal.result
        .then(function(movieObj) {
          if (dialogMode === vm.dialogModes.CREATE){
            movieObj.create().then(
              function(response){
                vm.movies.push(response);
              });
          }
          else if (dialogMode === vm.dialogModes.UPDATE){
            movieObj.update().then(
              function(response){
                var movieIndex = _.findIndex(vm.movies, function(movie)
                { return movie.id == response.id });
                vm.movies.splice(movieIndex, 1);
                vm.movies.push(response);
              });
          }
        });
    }

    function onDelete(movieId){
      var movieIndex = _.findIndex(vm.movies, function(movie)
      { return movie.id == movieId });
      var movieObj = new MoviesService.Movie(vm.movies[movieIndex]);
      var modal = $uibModal.open({
        templateUrl: '/media/movies/app/delete_modal.html',
        controller: 'DeleteNotifierCtrl',
        controllerAs: 'dialog',
        resolve: {
        }
      });
      modal.result.then(function(response) {
        if (response.isToBeDeleted){
          movieObj.delete();
          vm.movies.splice(movieIndex, 1);
          vm.message.text = 'Movie has been deleted successfully';
          vm.message.error = true;
          $timeout(function () {
            resetMessages();}, 5000);
        }
      });
    }

    function resetMessages(){
      vm.message.success = false;
      vm.message.error = false;
      vm.message.text = '';
    }

  }
})(window.angular, window._);