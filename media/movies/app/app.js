(function(angular) {
  angular.module('movies', [
    'ui.bootstrap'
  ]).config(configure);

  configure.$inject = ['$httpProvider'];
  function configure($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
  }

})(angular);
