
(function(angular, _) {
  angular.module('movies').controller('MoviesCtrl', [
    '$scope', '$uibModalInstance', 'movieInfo', 'dialogSettings', '$http', MoviesCtrl
  ]);
  function MoviesCtrl($scope, $uibModalInstance, movieInfo, dialogSettings, $http)
  {
    var vm = this;
    vm.movie = movieInfo;
    vm.dialogSettings = dialogSettings;
    vm.dialogModes = {
      CREATE : 1,
      UPDATE: 2
    };
    vm.errors = '';
    vm.cancel = function(){
      $uibModalInstance.dismiss('cancel');
    };

    vm.save = function(){
      vm.errors = vm.movie.validate();
      if (vm.errors.length == 0){
        $uibModalInstance.close(vm.movie);
      }
    };
  }

  angular.module('movies').controller('DeleteNotifierCtrl', [
    '$scope', '$uibModalInstance', DeleteNotifierCtrl
  ]);
  function DeleteNotifierCtrl($scope, $uibModalInstance)
  {
    var vm = this;

    vm.cancel = function(){
      $uibModalInstance.dismiss('cancel');
    };

    vm.save = function(){
      $uibModalInstance.close({isToBeDeleted: true});
    };
  }

})(window.angular, window._);
