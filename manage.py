
from django.core.management import execute_manager
import logging
logger = logging.getLogger(__name__)
import sys

import settings # Assumed to be in the same directory.
if __name__ == "__main__":
    try:
        execute_manager(settings)
    except BaseException as e:
        logger.error("management command failed to execute: %s error:%s" %(" ".join(sys.argv), str(e.args)))
        raise
