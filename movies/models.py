# Create your models here.
from django.db import models


class Movie(models.Model):
    """
    Class to store Movie objects
    """
    artists = models.CharField(max_length=800, null=True, blank=True)
    director_name = models.CharField(max_length=500, db_index=True)
    genre = models.TextField(null=True, blank=True, db_index=True)
    imdb_score = models.FloatField(default=0)
    name = models.CharField(max_length=800, db_index=True)
    popularity = models.FloatField(default=0, verbose_name='99popularity')
    releasing_on = models.DateField(auto_now_add=True)

    class Meta:
        unique_together = ('name', 'director_name', 'releasing_on')

    def __unicode__(self):
        return u'{} by: {} '.format(self.name, self.director_name)

    def save(self, *args, **kwargs):
        # Validate entry info before create or update record
        self.validate_movie_record()
        super(Movie, self).save(*args, **kwargs)

    def validate_movie_record(self):
        """
        Function to validate an entity before saved it into database.
        TODO: We can add more validation here like limit values of
        imdb_score, popularity etc.
        """
        if not self.name or not self.director_name:
            raise ValueError(u"Please enter name and director name for movie")
