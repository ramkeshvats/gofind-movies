
from django.conf.urls import patterns, url
from rest_framework import routers

from .views import MoviesViewSet, get_app_config, api_logout


router = routers.SimpleRouter()
router.register(r'movies', MoviesViewSet,
                base_name="Movies")

urlpatterns = patterns('',
        url(r'^$', get_app_config),
        url(r'^logout/$', api_logout)
)

urlpatterns += router.urls
