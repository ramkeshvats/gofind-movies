# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Movie.releasing_on'
        db.alter_column('movies_movie', 'releasing_on', self.gf('django.db.models.fields.DateField')(auto_now_add=True))

    def backwards(self, orm):

        # Changing field 'Movie.releasing_on'
        db.alter_column('movies_movie', 'releasing_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True))

    models = {
        'movies.movie': {
            'Meta': {'unique_together': "(('name', 'director_name', 'releasing_on'),)", 'object_name': 'Movie'},
            'artists': ('django.db.models.fields.CharField', [], {'max_length': '800', 'null': 'True', 'blank': 'True'}),
            'director_name': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'genre': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imdb_score': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '800', 'db_index': 'True'}),
            'popularity': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'releasing_on': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['movies']