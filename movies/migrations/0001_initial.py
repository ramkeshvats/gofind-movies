# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Movie'
        db.create_table('movies_movie', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('artists', self.gf('django.db.models.fields.CharField')(max_length=800, null=True, blank=True)),
            ('director_name', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('genre', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('imdb_score', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=800, db_index=True)),
            ('popularity', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('releasing_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal('movies', ['Movie'])

        # Adding unique constraint on 'Movie', fields ['name', 'director_name', 'releasing_on']
        db.create_unique('movies_movie', ['name', 'director_name', 'releasing_on'])


    def backwards(self, orm):
        # Removing unique constraint on 'Movie', fields ['name', 'director_name', 'releasing_on']
        db.delete_unique('movies_movie', ['name', 'director_name', 'releasing_on'])

        # Deleting model 'Movie'
        db.delete_table('movies_movie')


    models = {
        'movies.movie': {
            'Meta': {'unique_together': "(('name', 'director_name', 'releasing_on'),)", 'object_name': 'Movie'},
            'artists': ('django.db.models.fields.CharField', [], {'max_length': '800', 'null': 'True', 'blank': 'True'}),
            'director_name': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'genre': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imdb_score': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '800', 'db_index': 'True'}),
            'popularity': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'releasing_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['movies']