# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding index on 'Movie', fields ['director_name']
        db.create_index('movies_movie', ['director_name'])

        # Adding index on 'Movie', fields ['genre']
        db.create_index('movies_movie', ['genre'])


    def backwards(self, orm):
        # Removing index on 'Movie', fields ['genre']
        db.delete_index('movies_movie', ['genre'])

        # Removing index on 'Movie', fields ['director_name']
        db.delete_index('movies_movie', ['director_name'])


    models = {
        'movies.movie': {
            'Meta': {'unique_together': "(('name', 'director_name', 'releasing_on'),)", 'object_name': 'Movie'},
            'artists': ('django.db.models.fields.CharField', [], {'max_length': '800', 'null': 'True', 'blank': 'True'}),
            'director_name': ('django.db.models.fields.CharField', [], {'max_length': '500', 'db_index': 'True'}),
            'genre': ('django.db.models.fields.TextField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imdb_score': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '800', 'db_index': 'True'}),
            'popularity': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'releasing_on': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['movies']