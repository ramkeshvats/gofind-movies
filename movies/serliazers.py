
from rest_framework import serializers
from .models import Movie


class MovieSerializer(serializers.ModelSerializer):

    class Meta:
        model = Movie
        fields = (
            'director_name', 'genre', 'id', 'imdb_score', 'name',
            'popularity', 'releasing_on'
        )

    def create(self, validated_data):
        movie = Movie.objects.create(
            name=validated_data.get('name'),
            director_name=validated_data.get('director_name'),
            imdb_score=validated_data.get('imdb_score'),
            popularity=validated_data.get('popularity'),
            genre=validated_data.get('genre')
        )
        movie.save()
        self.object = movie
        return movie

    def update(self, movie, validated_data):
        movie.name = validated_data.get('name')
        movie.director_name = validated_data.get('director_name')
        movie.imdb_score = validated_data.get('imdb_score')
        movie.popularity = validated_data.get('popularity')
        movie.genre = validated_data.get('genre')
        movie.save()
        self.object = movie
        return movie