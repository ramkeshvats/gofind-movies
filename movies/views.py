
# Create your views here.
from django.shortcuts import render
from django.db.models import Q
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.renderers import JSONRenderer
from rest_framework import status

from .models import Movie
from .serliazers import MovieSerializer


@login_required
def get_app_config(request):
    is_staff = request.user.is_staff
    cxt = {
        'is_staff': is_staff
    }
    return render(request, 'movies/main.html', cxt)


def api_logout(request):
    logout(request)
    return HttpResponseRedirect("/accounts/login/")


# Rest framework view for movie objects.
class MoviesViewSet(viewsets.ModelViewSet):
    renderer_classes = (JSONRenderer, )
    serializer_class = MovieSerializer

    def get_queryset(self, search_query, page_number):
        """
        :param search_query: Movies search query(String)
        :param page_number: Used for pagination(Integer)
        :return: It returns a queryset of Movie based on the param passed.
        TODO: use pagination here.
        """

        # We can use SQL query will be a little bit faster. Other option like
        #  Solr or Redis queue(If our DB is very large)
        return Movie.objects.filter(
            Q(name__icontains=search_query) |
            Q(director_name__icontains=search_query) |
            Q(genre__icontains=search_query)
        ).order_by('name')

    # To return serialized movies objects based on query.
    def list(self, request):
        search_query = request.GET.get('q', '')
        page_number = request.GET.get('page', 1)
        serializer = MovieSerializer(
            self.get_queryset(search_query, page_number), many=True)
        return Response(
            {'movies_list': serializer.data,
             'is_staff_user': request.user.is_staff}
        )

    # To get a particular Movie
    def retrieve(self, request, pk=None):
        requested_movie = self.get_movie_object(pk)
        if not requested_movie:
            return Response(
                data={
                    'code': status.HTTP_404_NOT_FOUND,
                    'message': 'Movie not found, wrong request',
                    'fields': ''
                },
            )
        serializer = MovieSerializer(requested_movie)
        return Response(serializer.data)

    # To create new entry in DB.
    def create(self, request):
        if request.user.is_staff:
            try:
                serializer = MovieSerializer(data=request.DATA)
                movie = serializer.create(request.DATA)
                return Response(serializer.data)
            except Exception as e:
                print (u'Unknown Error: {}'.format(e))
                return Response(
                    data={
                        'code': status.HTTP_500_INTERNAL_SERVER_ERROR,
                        'message': 'Internal Error',
                        'fields': ''
                    }
                )
        else:
            return Response(
                    data={
                        'code': status.HTTP_405_METHOD_NOT_ALLOWED,
                        'message': 'Not allowed, Thanks',
                        'fields': ''
                    }
            )

    # To update existing entity.
    def update(self, request, pk=None):
        if request.user.is_staff:
            requested_movie = self.get_movie_object(pk)
            if not requested_movie:
                return Response(
                    data={
                        'code': status.HTTP_404_NOT_FOUND,
                        'message': 'Requested Movie not found, wrong request'
                    }
                )

            serializer = MovieSerializer(requested_movie, data=request.DATA)
            updated_movie = serializer.update(requested_movie, request.DATA)

            return Response(serializer.data)
        else:
            return Response(
                    data={
                        'code': status.HTTP_405_METHOD_NOT_ALLOWED,
                        'message': 'Not allowed, Thanks',
                        'fields': ''
                    }
            )

    # To delete an entity from DB.
    def destroy(self, request, pk=None):
        if request.user.is_staff:
            requested_movie = self.get_movie_object(pk)
            if not requested_movie:
                return Response(
                    data={
                        'code': status.HTTP_404_NOT_FOUND,
                        'message': 'Requested Movie not found, wrong request',
                        'fields': ''},
                    status=400
                )

            requested_movie.delete()
            return Response()
        else:
            return Response(
                    data={
                        'code': status.HTTP_405_METHOD_NOT_ALLOWED,
                        'message': 'Not allowed, Thanks',
                        'fields': ''
                    }
            )

    def get_movie_object(self, id):
        """
        :param id: id for requested Movie(integer)
        :return: Object of Movie if exists else will return None
        """
        try:
            movie = Movie.objects.get(id=id)
        except Movie.DoesNotExist:
            movie = None
        return movie

